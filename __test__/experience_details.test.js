// const req = require("supertest");
// const url = "http://localhost:5050/api/v1/admin/experienceDetails";

// describe("post /create experience ", function () {
    

//     it.only("create experience positive testcase", async () => {
//         const body = await req(url).post("/createExperienceDetails")
//         .field("studentId", "39")
//         .field("expType", "Experience")
//         .field("companyName", "s2p")
//         .field("fromDate", "2019/06/12")
//         .field("toDate", "2019/06/12")
//         .field("salary", "44lakh")
//         .field("key", "education")
//         .field("certificate", "server\assets\images\download (1).png")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(200); 
//         console.log(body);

//     })

//     //path wrong
//     it("create experience negative testcase", async () => {
//         const body = await req(url).post("/createExperienceDetais")
//         .field("studentId", "39")
//         .field("expType", "Experience")
//         .field("companyName", "s2p")
//         .field("fromDate", "2019/06/12")
//         .field("toDate", "2019/06/12")
//         .field("salary", "44lakh")
//         .field("key", "education")
//         .field("certificate", "server\assets\images\download (1).png")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(404); 
//     })

//     //token not send
//     it("create experience negative testcase", async () => {
//         const body = await req(url).post("/createExperienceDetails")
//         .field("studentId", "39")
//         .field("expType", "Experience")
//         .field("companyName", "s2p")
//         .field("fromDate", "2019/06/12")
//         .field("toDate", "2019/06/12")
//         .field("salary", "44lakh")
//         .field("key", "education")
//         .field("certificate", "server\assets\images\download (1).png")
        
//         expect(body.status).toBe(401); 
//     })


//     //blank data 
//     it("create experience negative testcase", async () => {
//         const body = await req(url).post("/createExperienceDetails")
  
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(400); 
//     })

// });

// describe("get /get all experience ", function () {
    

//     it("get experience details positive testcase", async () => {
//         const body = await req(url).get("/getExperienceDetails?page=1&pagesize=10&search=")
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(200); 
//     })

//     //path wrong
//     it("get experience details negative testcase", async () => {
//         const body = await req(url).get("/getExpernceDetails?page=1&pagesize=10&search=")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(404); 
//     })

//     //token not send
//     it("get experience details negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetails?page=1&pagesize=10&search=")
        
//         expect(body.status).toBe(401); 
//     })

// });


// describe("get /get experience by ID ", function () {
    

//     it("get experience by id positive testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsById/10")
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(200); 
//     })

//     //path wrong
//     it("get experience by id negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailById/10")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(404); 
//     })

//     //token not send
//     it("get experience by id negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsById/10")
        
//         expect(body.status).toBe(401); 
//     })

//     //id not passed
//     it("get experience by id negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsById/")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(404); 
//     })
    
    
//     //invalid id 
//     it("get experience by id negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsById/4515")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(400); 
//     })

// });


// describe("get /get experience by student id ", function () {
    

//     it("get experience by student id positive testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsByStudentId/5")
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(200); 
//     })

//     //path wrong
//     it("get experience by student id negative testcase", async () => {
//         const body = await req(url).get("/getExerienceDetailsByStudentId/5")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(404); 
//     })

//     //token not send
//     it("get experience by student id negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsByStudentId/5")
        
//         expect(body.status).toBe(401); 
//     })

//     //id not passed
//     it("get experience by student id negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsByStudentId/")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(404); 
//     })
    
    
//     //invalid id 
//     it("get experience by student id negative testcase", async () => {
//         const body = await req(url).get("/getExperienceDetailsByStudentId/456122")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(400); 
//     })

// });


// describe("delete /experience", function () {
    

//     it("delete experience details positive testcase", async () => {
//         const body = await req(url).delete("/deleteExperienceDetails/1")
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
         
//         expect(body.status).toBe(200); 
//     })


// });