// const req = require("supertest");
// const url = "http://localhost:5050/api/v1/admin";


// describe("post /taluka ", function () {
//     it("Create Taluka positive testcase", async () => {
//         const body = await req(url).post("/taluka/createTaluka").send([
               
//                 { "stateId": 21, "talukaName": "Kampt", "districtId": 368 }
              
//             ]).set('Authorization', 'bearer ' + process.env.TOKEN);
        
//         expect(body.status).toBe(200); 
//         if(body.status == 200){
//             console.log("Create Taluka positive testcase =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
        
//     }),

//     it("Create Taluka NEGATIVE testcase", async () => {
//         const body = await req(url).post("/taluka/createTaluka").send([
               
//                 { "stateId": 21, "talukaName": 45446546 , "districtId": 368 }

                
              
//             ]).set('Authorization', 'bearer ' + process.env.TOKEN);


//         expect(body.status).toBe(200); 
//         if(body.status == 200){
//             console.log("Create Taluka NEGATIVE testcase =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Invalid path
//     it("Create Taluka negative testcase", async () => {
//         const body = await req(url).post("/taluka/createTal").send([
               
//                 { "stateId": 21, "talukaName": "KAMPTEE" , "districtId": 368 }

                
              
//             ]).set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(404); 
//         if(body.status == 404){
//             console.log("Create Taluka negative testcase =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Not passing token
//     it("Create Taluka negative testcase", async () => {
//         const body = await req(url).post("/taluka/createTaluka").send([
               
//                 { "stateId": 21, "talukaName": "KAMPTEE" , "districtId": 368 }       
              
//             ])

//         expect(body.status).toBe(401); 
//         if(body.status == 401){
//             console.log("Create Taluka negative testcase =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Student ID not passed
//     it("Create Taluka negative testcase", async () => {
//         const body = await req(url).post("/taluka/createTaluka").send([
               
//                 { "talukaName": "KAMPTEE" , "districtId": 368 }       
              
//             ]).set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(500); 
//         if(body.status == 500){
//             console.log("Create Taluka negative testcase, ID not passed =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     })

  
// });

// describe("post /taluka ", function () {
    

//     it("get taluka by id positive testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaById/4").set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(200); 
//         if(body.status == 200){
//             console.log("get taluka by id positive testcase =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Not passed ID
//     it("get taluka by id Negative testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaById/").set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(404); 
//         if(body.status == 404){
//             console.log("get taluka by id Negative testcase, Not passed ID =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Token is not passing
//     it("get taluka by id negative testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaById/4")

//         expect(body.status).toBe(401); 
//         if(body.status == 401){
//             console.log("get taluka by id positive testcase, Token is not passing =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Invalid ID
//     it("get taluka by id negative testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaById/400").set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(400); 
//         if(body.status == 400){
//             console.log("get taluka by id positive testcase, Invalid ID =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     })


// });


// describe("post /taluka ", function () {
    

//     it("get by stateId and districtId positive testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaByStateIdAndDistrictId?stateId=21&districtId=368/getTalukaByStateIdAndDistrictId?stateId=21&districtId=368").set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(200); 
//         if(body.status == 200){
//             console.log("get by stateId and districtId positive testcase =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Not passing student ID and DistrictID
//     it("get by stateId and districtId Negative testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaByStateIdAndDistrictId?stateId=&districtId=").set('Authorization', 'bearer ' + process.env.TOKEN);

//         expect(body.status).toBe(400); 
//         if(body.status == 400){
//             console.log("get by stateId and districtId Negative testcase, Not passing student ID and DistrictID =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),

//     //Token is not passing 
    
//     it("get taluka by id negative testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaByStateIdAndDistrictId?stateId=21&districtId=368")

//         expect(body.status).toBe(401); 
//         if(body.status == 401){
//             console.log("get taluka by id positive testcase, Token is not passing =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     }),
    

//     //Invalid ID
//     it("get by stateId and districtId negative testcase", async () => {
//         const body = await req(url).get("/taluka/getTalukaByStateIdAndDistrictId?stateId=666&districtId=7676").set('Authorization', 'bearer ' + process.env.TOKEN);
        
//         expect(body.status).toBe(200); 
//         if(body.status == 200){
//             console.log("get by stateId and districtId positive testcase, Invalid ID =>",body._body);
//         }else{
//             console.log(body.error.text);
//         }
//     })

// });



