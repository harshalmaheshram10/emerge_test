// const req = require("supertest")

// const url = "http://localhost:5050/api/v1/admin/diplomaDetails"

// let id;

// describe("post /create diplomaDetails", function () {
    
//     it("diploma positive testcase", async () => {
//         const body = await req(url).post("/createDiplomaDetails")
//         .field("education", "Diploma")
//         .field("studentId", "5")
//         .field("stateId", "1")
//         .field("districtId", "1")
//         .field("collegeId", "11")
//         .field("percentage", "67")
//         .field("yearOfPassing", "2018")
//         .field("stream", "Diploma In Agriculture")
//         .field("key", "education")
//         .field("certificate", "server\assets\images\download (1).png")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
                        
//         expect(body.status).toBe(200); 
//         id = body._body.result.id;
//     })

//     //Not passing all data
//     it("diploma negative testcase", async () => {
//         const body = await req(url).post("/createDiplomaDetails")
//         .field("education", "Diploma")
//         .field("studentId", "5")
//         .field("percentage", "67")
//         .field("yearOfPassing", "2018")
//         .field("stream", "Diploma In Agriculture")
//         .field("key", "education")
//         .field("certificate", "server\assets\images\download (1).png")
        
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
        
//         expect(body.status).toBe(400); 
        
//     })

//     //path is wrong
//     it("diploma negative testcase", async () => {
//         const body = await req(url).post("/createDiplomaDetai")
//         .field("education", "Diploma")
//         .field("studentId", "5")
//         .field("stateId", "1")
//         .field("districtId", "1")
//         .field("collegeId", "11")
//         .field("percentage", "67")
//         .field("yearOfPassing", "2018")
//         .field("stream", "Diploma In Agriculture")
//         .field("key", "education")
//         .field("certificate", "server\assets\images\download (1).png")
//         .set('Authorization', 'bearer ' + process.env.TOKEN);


                
//         expect(body.status).toBe(404); 
//     })

//     //Token is not set
//     it("diploma negative testcase", async () => {
//         const body = await req(url).post("/createDiplomaDetails")
//         .field("education", "Diploma")
//         .field("studentId", "5")
//         .field("stateId", "1")
//         .field("districtId", "1")
//         .field("collegeId", "11")
//         .field("percentage", "67")
//         .field("yearOfPassing", "2018")
//         .field("stream", "Diploma In Agriculture")
//         .field("key", "education")
//         .field("certificate", "server\assets\images\download (1).png")
                
//         expect(body.status).toBe(401); 
//     })
    

// });

// describe("post /get diplomaDetails", function () {
    

//     it("get diploma positive testcase", async () => {
//         const body = await req(url).get("/getAllDiplomaDetails")
    
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
      
                
//         expect(body.status).toBe(200); 
//     })

//     //path is wrong
//     it("get diploma negative testcase", async () => {
//         const body = await req(url).get("/getAllDiplomaDetail")
    
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
                
//         expect(body.status).toBe(404); 
//     })

//     //Token is not set
//     it("get diploma negative testcase", async () => {
//         const body = await req(url).get("/getAllDiplomaDetails");

//         expect(body.status).toBe(401);
//     });    

// });

// describe("post /get by id", function () {
    

//     it("get diploma by Student ID positive testcase", async () => {
//         const body = await req(url)
//             .get("/getDiplomaDetailsById/"+id.toString())

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(200);
//     });

//     //path is wrong
//     it("get diploma by Student ID negative testcase", async () => {
//         const body = await req(url)
//             .get("/getDiplomaDetailsById/")

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(404);
//     });

//     //Invalid id passing
//     it("get diploma by Student ID negative testcase", async () => {
//         const body = await req(url)
//             .get("/getDiplomaDetailsById/5")

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(400);
//         console.log(body.text);
//     });

//     //Token is not set
//     it("get diploma by Student ID negative testcase", async () => {
//         const body = await req(url).get("/getDiplomaDetailsById/3");

//         expect(body.status).toBe(401);
//     });    

// });

// describe("post /get student by id", function () {
    

//     it("get student by ID positive testcase", async () => {
//         const body = await req(url).get("/getDiplomaDetailsByStudentId/5")
    
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
      
                
//         expect(body.status).toBe(200); 
//     })

//     //path is wrong
//     it("get student by ID negative testcase", async () => {
//         const body = await req(url).get("/getDiplomaDetailsByI")
    
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
                
//         expect(body.status).toBe(404); 
//     })

//     //Invalid id passing
//     it("get student by ID negative testcase", async () => {
//         const body = await req(url).get("/getDiplomaDetailsByStudentId/80")
    
//         .set('Authorization', 'bearer ' + process.env.TOKEN);
                
//         expect(body.status).toBe(400); 
//         console.log(body.text);
//     })

//     //Token is not set
//     it("get student by ID negative testcase", async () => {
//         const body = await req(url).get("/getDiplomaDetailsByStudentId/5")
        
//         expect(body.status).toBe(401); 
//     })    

// });

// describe("update /update diploma details", function () {
//     it("update student positive testcase", async () => {
//         const body = await req(url)
//             .put("/updateDiplomaDetails/"+id.toString())
//             .field("education", "Diploma")
//             .field("studentId", "5")
//             .field("stateId", "1")
//             .field("districtId", "1")
//             .field("collegeId", "11")
//             .field("percentage", "67")
//             .field("yearOfPassing", "2018")
//             .field("stream", "Diploma In Agriculture")
//             .field("key", "education")
//             .field("certificate", "serverassetsimagesdownload (1).png")

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(200);
//     });

//     // path is wrong
//     it("update student negative testcase", async () => {
//         const body = await req(url)
//             .put("/updateDiplomaDetais/7")
//             .field("education", "Diploma")
//             .field("studentId", "5")
//             .field("stateId", "1")
//             .field("districtId", "1")
//             .field("collegeId", "11")
//             .field("percentage", "67")
//             .field("yearOfPassing", "2018")
//             .field("stream", "Diploma In Agriculture")
//             .field("key", "education")
//             .field("certificate", "serverassetsimagesdownload (1).png")

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(404);
//     });

//     // //Invalid id passing
//     it("update student negative testcase", async () => {
//         const body = await req(url)
//             .put("/updateDiplomaDetails/80")
//             .field("education", "Diploma")
//             .field("studentId", "5")
//             .field("stateId", "1")
//             .field("districtId", "1")
//             .field("collegeId", "11")
//             .field("percentage", "67")
//             .field("yearOfPassing", "2018")
//             .field("stream", "Diploma In Agriculture")
//             .field("key", "education")
//             .field("certificate", "assetsimagesdownload (1).png")

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(500);
//     });

//     it("update student negative testcase", async () => {
//         const body = await req(url)
//             .put("/updateDiplomaDetails/"+id.toString())
//             .field("education", "Diploma")
//             .field("studentId", "5")
//             .field("stateId", "1")
//             .field("districtId", "1")
//             .field("collegeId", "11")
//             .field("percentage", "67")
//             .field("yearOfPassing", "2018")
//             .field("stream", "Diploma In Agriculture")
//             .field("key", "education")
//             .field("certificate", "assetsimagesdownload (1).png")


//         expect(body.status).toBe(401);
//     });
// });

// describe("delete /delete diploma details", function () {
//     it("delete diploma positive testcase", async () => {
//         const body = await req(url)
//             .delete("/deleteDiplomaDetails/" + id.toString())

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(200);
//     });

//     //wrong Id
//     it("delete diploma negative testcase", async () => {
//         const body = await req(url)
//             .delete("/deleteDiplomaDetails/456")

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(500);
//     });

//     //token not send
//     it("delete diploma negative testcase", async () => {
//         const body = await req(url).delete("/deleteDiplomaDetails/"+id.toString());

//         expect(body.status).toBe(401);
//     });

//     //path wrong
//     it("delete diploma negative testcase", async () => {
//         const body = await req(url).delete("/deleteDiplomDetails/"+id.toString())

//         .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(404);
//     });
// });


