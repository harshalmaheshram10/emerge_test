const req = require("supertest");
const url = "http://localhost:5050/api/v1/admin";

let num = Math.floor(Math.random() * 9999999999) + 1000000000;

let num1 = Math.floor(Math.random() * 9999999999) + 1000000000;

let id;

// describe("post /create student", function () {
//     beforeAll(() => {
//         it("should be create state", async () => {
//             const body = await req(url)
//                 .post("/state/createState")
//                 .send([
//                     {
//                         stateName: "Maharashtra",
//                     },
//                 ])

//                 .set("Authorization", "bearer " + process.env.TOKEN);
//             expect(body.status).toBe(200);
//             console.log(body);
//         });

//         it("should be create district", async () => {
//             const body = await req(url)
//                 .post("/district/createDistrict")
//                 .send([
//                     {
//                         stateId: 1,
//                         districtName: "Nagpur",
//                         role: "SUPER ADMIN",
//                     },
//                 ])
//                 .set("Authorization", "bearer " + process.env.TOKEN);
//             expect(body.status).toBe(200);
//             console.log(body);
//         });

//         it("should be taluka", async () => {
//             const body = await req(url)
//                 .post("/taluka/createTaluka")
//                 .send([
//                     {
//                         stateId: 1, 
//                         talukaName: "Kampti", 
//                         districtId: 1
//                     }
//                 ])
//                 .set("Authorization", "bearer " + process.env.TOKEN);
//             expect(body.status).toBe(200);
//             console.log(body);
//         });
//     });

//     it("Create student positive testcase", async () => {
//         const body = await req(url)
//             .post("/studentDetails/createStudentDetails")
//             .field("key", "student")
//             .field("name", "Harsh")
//             .field("dob", "2000-1-4")
//             .field("email", "sh3426" + num.toString() + "@gmail.com")
//             .field("mobile", num.toString())
//             .field("stateId", "1")
//             .field("districtId", "1")
//             .field("aadharNo", num.toString() + "12")
//             .field("talukaId", "1")
//             .field("role", "SUPER ADMIN")
//             .field("highestQualification", "diploma")
//             .field("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)")

//             .set("Authorization", "bearer " + process.env.TOKEN);

//         if (body.status == 200) {
//             console.log("Create student positive testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//         id = body._body.result.studentId;

//         expect(body.status).toBe(200);
//     }),
//         it("Create student negative testcase", async () => {
//             const body = await req(url)
//                 .post("/studentDetails/createStudentDetails")
//                 .field("key", "student")
//                 .field("name", "Harsh")
//                 .field("dob", "2000-1-4")
//                 .field("email", "sh3426" + num.toString() + "@gmail.com")
//                 .field("mobile", num.toString())
//                 .field("stateId", "21")
//                 .field("districtId", "1")
//                 .field("aadharNo", num.toString() + "12")
//                 .field("talukaId", "1")
//                 .field("role", "SUPER ADMIN")
//                 .field("highestQualification", "diploma")
//                 .field("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)")

//                 .set("Authorization", "bearer " + process.env.TOKEN);
//             if (body.status == 400) {
//                 console.log("Create student negative testcase =>", body._body);
//             } else {
//                 console.log(body.error.text);
//             }

//             expect(body.status).toBe(400);
//         }),
//         it("Create student negative testcase", async () => {
//             const body = await req(url)
//                 .post("/studentDetails/createStudentDetails")
//                 .field("key", "student")
//                 .field("name", "Harsh")
//                 .field("dob", "2000-1-4")
//                 .field("email", "sh3426" + num.toString() + "@gmail.com")
//                 .field("mobile", num.toString())
//                 .field("stateId", "21")
//                 .field("districtId", "1")
//                 .field("aadharNo", num.toString() + "12")
//                 .field("talukaId", "1")
//                 .field("role", "SUPER ADMIN")
//                 .field("highestQualification", "diploma")
//                 .field("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)");

//             if (body.status == 401) {
//                 console.log("Create student negative testcase, Authorization not set =>", body._body);
//             } else {
//                 console.log(body.error.text);
//             }

//             expect(body.status).toBe(401);
//         }),
//         it("Create student negative testcase", async () => {
//             const body = await req(url)
//                 .post("/studentDetails/createStudentDetails")
//                 .field("key", "student")
//                 .field("name", "Harsh")
//                 .field("dob", "2000-1-4")
//                 .field("email", "sh3426" + num.toString() + "@gmail.com")

//                 .field("stateId", "21")
//                 .field("districtId", "1")

//                 .field("talukaId", "1")
//                 .field("role", "SUPER ADMIN")
//                 .field("highestQualification", "diploma")
//                 .field("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)")
//                 .set("Authorization", "bearer " + process.env.TOKEN);

//             if (body.status == 400) {
//                 console.log("Create student negative testcase, required filled not passed =>", body._body);
//             } else {
//                 console.log(body.error.text);
//             }

//             expect(body.status).toBe(400);
//         });
// });

// describe("post /update studentDetails", function () {
//     it("update student postive testcase", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateStudentDetails/5")
//             .field("key", "student")
//             .field("name", "Harshu")
//             .field("dob", "2000-1-4")
//             .field("email", "adf" + num1.toString() + "hjahkj@gmail.com")
//             .field("mobile", num1.toString())
//             .field("stateId", "21")
//             .field("districtId", "1")
//             .field("aadharNo", num1.toString() + "96")
//             .field("talukaId", "1")
//             .field("role", "SUPER ADMIN")
//             .field("highestQualification", "diploma")
//             .set("Authorization", "bearer " + process.env.TOKEN)
//             .set("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)");

//         if (body.status == 200) {
//             console.log("upadate student negative testcase,  =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }

//         expect(body.status).toBe(200);
//     });

//     it("update student negative testcase", async () => {
//         const body = await req(url)
//             .put("/studentDetail/updateStudentDetails/5")
//             .field("key", "student")
//             .field("name", "Harshu")
//             .field("dob", "2000-1-4")
//             .field("email", "adf" + num1.toString() + "hjahkj@gmail.com")
//             .field("mobile", num1.toString())
//             .field("stateId", "21")
//             .field("districtId", "1")
//             .field("aadharNo", num1.toString() + "96")
//             .field("talukaId", "1")
//             .field("role", "SUPER ADMIN")
//             .field("highestQualification", "diploma")
//             .set("Authorization", "bearer " + process.env.TOKEN)
//             .set("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)");

//         if (body.status == 404) {
//             console.log("upadate student negative testcase, wrong path =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }

//         expect(body.status).toBe(404);
//     });

//     it("update student negative testcase", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateStudentDetails")
//             .field("key", "student")
//             .field("name", "Harshu")
//             .field("dob", "2000-1-4")
//             .field("email", "adf" + num1.toString() + "hjahkj@gmail.com")
//             .field("mobile", num1.toString())
//             .field("stateId", "21")
//             .field("districtId", "1")
//             .field("aadharNo", num1.toString() + "96")
//             .field("talukaId", "1")
//             .field("role", "SUPER ADMIN")
//             .field("highestQualification", "diploma")
//             .set("Authorization", "bearer " + process.env.TOKEN)
//             .set("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)");

//         if (body.status == 404) {
//             console.log("upadate student negative testcase, Id not send =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }

//         expect(body.status).toBe(404);
//     });

//     // it("update student negative testcase", async () => {
//     //     const body = await req(url)
//     //         .put("/studentDetails/updateStudentDetails/200")
//     //         .field("key", "student")
//     //         .field("name", "gaya")
//     //         .field("dob", "2019/06/12")
//     //         .field("email", "gayajj@gmail.com")
//     //         .field("mobile", "4367356987")
//     //         .field("stateId", "21")
//     //         .field("districtId", "1")
//     //         .field("aadharNo", "98786545676596")
//     //         .field("gender", "Female")
//     //         .field("talukaId", "1")
//     //         .field("status", "Inactive")
//     //         .field("highestQualification", "diploma")
//     //         .set("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)")
//     //         .set("Authorization", "bearer " + process.env.TOKEN)

//     //     if(body.status == 400){
//     //         console.log("upadate student negative testcase, Id not send =>",body._body);
//     //     }else{
//     //         console.log(body.error.text);
//     //     }

//     //     expect(body.status).toBe(400);

//     // });
// });

// describe("post /get Student list", function () {
//     it("get student list positive testcase", async () => {
//         const body = await req(url)
//             .get("/studentDetails/getAllStudentDetails")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(200);
//         if (body.status == 200) {
//             console.log("Create student negative testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get student list negative testcase", async () => {
//         const body = await req(url)
//             .get("/studenDetails/getAllStudentDetails")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("Create student negative testcase, wrong path =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get student list negative testcase", async () => {
//         const body = await req(url).get("/studentDetails/getAllStudentDetails");

//         expect(body.status).toBe(401);

//         if (body.status == 401) {
//             console.log("Create student negative testcase, token is not set =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });
// });

// describe("put /update highest qualification", function () {
//     it("put positive testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateHigherEducation/" + id.toString())
//             .send({
//                 highestQualification: "ITI",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(200);
//         if (body.status == 200) {
//             console.log("update highestQualification negative testcase, token is not set =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put negative testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateHigherEducation/")
//             .send({
//                 highestQualification: "ITI",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("update highestQualification negative testcase, id not passed =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put negative testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateHigherEducation/676")
//             .send({
//                 highestQualification: "ITI",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(400);
//         if (body.status == 400) {
//             console.log("update highestQualification negative testcase, invalid id =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put negative testcases", async () => {
//         const body = await req(url).put("/studentDetails/updateHigherEducation/53").send({
//             highestQualification: "ITI",
//         });
//         expect(body.status).toBe(401);
//         if (body.status == 401) {
//             console.log("update highestQualification negative testcase, token not set =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });
// });

// describe("put /update after education", function () {
//     it("put positive testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateAfterEdu/" + id.toString())
//             .send({
//                 afterEdu: "12th",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(200);
//         if (body.status == 401) {
//             console.log("update afterEdu negative testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put negative testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateAfterEdu/")
//             .send({
//                 afterEdu: "12th",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("update afterEdu negative testcase, id not passed =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put negative testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateAfterEdu/765")
//             .send({
//                 afterEdu: "12th",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(400);
//         if (body.status == 400) {
//             console.log("update afterEdu negative testcase, wrong id =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put negative testcases", async () => {
//         const body = await req(url).put("/studentDetails/updateAfterEdu/5").send({
//             afterEdu: "12th",
//         });

//         expect(body.status).toBe(401);
//         if (body.status == 401) {
//             console.log("update afterEdu negative testcase, token not set =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });
// });

// describe("put /update experience type", function () {
//     it("put positive testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateExperienceType/" + id.toString())
//             .send({
//                 experienceType: "experience",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(200);
//         if (body.status == 200) {
//             console.log("update experienceType negative testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put positive testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateExperienceType/")
//             .send({
//                 experienceType: "experience",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("update experienceType negative testcase, id not passed =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put positive testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetls/updateExperienceType/")
//             .send({
//                 experienceType: "experience",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("update experienceType negative testcase, wrong path =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("put positive testcases", async () => {
//         const body = await req(url)
//             .put("/studentDetails/updateExperienceType/")
//             .send({
//                 experienceType: "experience",
//             })
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("update experienceType negative testcase, token not set =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });
// });

// describe("Delete /delete student details", function () {
//     it("delete student negative testcases", async () => {
//         const body = await req(url)
//             .delete("/studentDetails/deleteStudentDetails/" + id.toString())
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(200);
//         if (body.status == 200) {
//             console.log("delete student negative testcase, student does not exist =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("delete student negative testcases", async () => {
//         const body = await req(url)
//             .delete("/studentDetails/deleteStudentDetails/65")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(500);
//         if (body.status == 500) {
//             console.log("delete student negative testcase, student does not exist =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("delete student negative testcases", async () => {
//         const body = await req(url)
//             .delete("/studentDetais/deleteStudentDetails/65")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("delete student negative testcase, path wrong =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });
// });

// describe("get /get all details by id", function () {
//     it("get positive testcases", async () => {
//         const body = await req(url)
//             .get("/studentDetails/getStudentAllDetailsById/5")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(200);
//         if (body.status == 200) {
//             console.log("get all details by id positive testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get negative testcases", async () => {
//         const body = await req(url)
//             .get("/studentDetaik/getStudentAllDetailsById/")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("delete student negative testcase, path wrong =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get negative testcases", async () => {
//         const body = await req(url)
//             .get("/studentDetails/getStudentAllDetailsById/687")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(400);
//         if (body.status == 400) {
//             console.log("delete student negative testcase, path wrong =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get negative testcases", async () => {
//         const body = await req(url).get("/studentDetails/getStudentAllDetailsById/687");

//         expect(body.status).toBe(401);
//         if (body.status == 401) {
//             console.log("delete student negative testcase, token not set =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });
// });

// describe("get /get by id", function () {
//     it("get positive testcases", async () => {
//         const body = await req(url)
//             .get("/studentDetails/getStudentDetailsById/5")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(200);
//         if (body.status == 200) {
//             console.log("get by id positive testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get negative testcases", async () => {
//         const body = await req(url)
//             .get("/studentDetails/getStudentDetailsById/")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("get by id positive testcase, token not set =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get negative testcases", async () => {
//         const body = await req(url)
//             .get("/studentDetals/getStudentDetailsById/5")
//             .set("Authorization", "bearer " + process.env.TOKEN);
//         expect(body.status).toBe(404);
//         if (body.status == 404) {
//             console.log("get by id positive testcase, invalid path =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get negative testcases", async () => {
//         const body = await req(url).get("/studentDetails/getStudentDetailsById/5");

//         expect(body.status).toBe(401);
//         if (body.status == 401) {
//             console.log("get by id positive testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });

//     it("get negative testcases", async () => {
//         const body = await req(url)
//             .get("/studentDetails/getStudentDetailsById/877")
//             .set("Authorization", "bearer " + process.env.TOKEN);

//         expect(body.status).toBe(400);
//         if (body.status == 400) {
//             console.log("get by id positive testcase =>", body._body);
//         } else {
//             console.log(body.error.text);
//         }
//     });
// });

// key: student
// name: Faizan
// dob: 2000-1-4
// email: faizan1@gmai.com
// mobile: 7978645421

// gender: Male
// stateId: 21
// districtId: 386
// aadharNo: 454564521321
// status: Active
// hiredStatus: false
// talukaId: 168
// role: STUDENT


describe('StudentDetails', () => {
    it("should be create state first", async () => {
        const body = await req(url)
            .post("/state/createState")
            .send([
                {
                    stateName: "Maharashtra",
                },
            ])
            .set("Authorization", "bearer " + process.env.TOKEN);
        expect(body.status).toBe(200);
    });

    it("should be create district", async () => {
        const body = await req(url)
            .post("/district/createDistrict")
            .send([
                {
                    stateId: 1,
                    districtName: "Nagpur",
                    role: "SUPER ADMIN",
                },
            ])
            .set("Authorization", "bearer " + process.env.TOKEN);
        expect(body.status).toBe(200);
    });

    it("should be create taluka", async () => {
        const body = await req(url)
            .post("/taluka/createTaluka")
            .send([
                {
                    stateId: 1, 
                    talukaName: "Kampti", 
                    districtId: 1
                }
            ])
            .set("Authorization", "bearer " + process.env.TOKEN);
        expect(body.status).toBe(200);
    });

    describe('create student', () => {
        describe('Student added succesfully',()=>{
            it("should be return 200", async () => {
                const body = await req(url)
                    .post("/studentDetails/createStudentDetails")
                    .field("key", "student")
                    .field("name", "Harsh")
                    .field("dob", "2000-1-4")
                    .field("email", "sh3426" + num.toString() + "@gmail.com")
                    .field("mobile", num.toString())
                    .field("stateId", "1")
                    .field("districtId", "1")
                    .field("aadharNo", num.toString() + "12")
                    .field("talukaId", "1")
                    .field("role", "SUPER ADMIN")
                    .field("highestQualification", "diploma")
                    .field("image", "server/assets/studentImage/1659701557734-bezkoder-download (1)")
                
                    .set("Authorization", "bearer " + process.env.TOKEN);
                
                
                id = body._body.result.studentId;
                
                expect(body.status).toBe(200);
                expect(body._body.result).toBe(body._body.result);
            })
        })  
    });
});

    